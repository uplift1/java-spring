FROM maven:3.6.1-jdk-8 as maven_builder
ARG MAVEN_OPTS
WORKDIR /app
ADD app/ .

RUN mvn clean package

FROM tomcat:8.0-alpine

LABEL maintainer=”deepak@softwareyoga.com”

COPY --from=maven_builder /app/target/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/demo.war

EXPOSE 8080

CMD ["catalina.sh", "run"]
